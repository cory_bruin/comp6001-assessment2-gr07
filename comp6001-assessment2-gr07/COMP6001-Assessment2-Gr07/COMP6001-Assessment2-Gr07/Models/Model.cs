﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace COMP6001_Assessment2_Gr07.Models
{   
   public class CryptoContext : DbContext
    {
    public CryptoContext(DbContextOptions<CryptoContext> options)
        : base(options)
    { }

    public DbSet<User> Users { get; set; }
    public DbSet<Holding> Holdings { get; set; }
    public DbSet<Wallet> Wallets { get; set; }
    }

    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }

    public class Holding
    {
        [Key]
        public int PurchaseId { get; set; }
        public string Symbol { get; set; }
        public double AmountHeld { get; set; }
        public double PurchasePrice { get; set; }
        public string Username { get; set; }

    }
    public class Wallet
    {
        [Key]
        public string Username { get; set; }
        public double CashAvailable { get; set; }
    }
}
