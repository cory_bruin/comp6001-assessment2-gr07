﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using COMP6001_Assessment2_Gr07.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace COMP6001_Assessment2_Gr07.Controllers
{

    public class UsersController : Controller
    {
        private readonly CryptoContext _context;
        const string loggedIn1 = "username";

        public UsersController(CryptoContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var walletValue = await _context.Wallets.SingleOrDefaultAsync(m => m.Username == HttpContext.Session.GetString(loggedIn1));
            if (walletValue != null)
            {
                ViewBag.Wallet = walletValue.CashAvailable;
                ViewBag.Message = HttpContext.Session.GetString(loggedIn1);
            }
            return View(await _context.Holdings.ToListAsync());
        }
        public async Task<IActionResult> Sell()
        {
            var walletValue = await _context.Wallets.SingleOrDefaultAsync(m => m.Username == HttpContext.Session.GetString(loggedIn1));
            if (walletValue != null)
            {
                ViewBag.Wallet = walletValue.CashAvailable;
                ViewBag.Message = HttpContext.Session.GetString(loggedIn1);
            }
            return View(await _context.Holdings.ToListAsync());
        }

        public IActionResult Register()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Welcome()
        {
            ViewBag.Message = HttpContext.Session.GetString(loggedIn1);
            return View();
        }
        public async Task<IActionResult> Buy(string test)
        {
            var walletValue = await _context.Wallets.SingleOrDefaultAsync(m => m.Username == HttpContext.Session.GetString(loggedIn1));
            if (walletValue != null)
            {
                ViewBag.Wallet = walletValue.CashAvailable;
                ViewBag.Message = HttpContext.Session.GetString(loggedIn1);
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Purchase(string buySym, double buyQTY, double buyPrice)
        {
            var buyTotal = buyPrice * buyQTY;
            var user = HttpContext.Session.GetString(loggedIn1);
            var walletConnection = await _context.Wallets.SingleOrDefaultAsync(m => m.Username == HttpContext.Session.GetString(loggedIn1));
            var walletValue = walletConnection.CashAvailable;
            if (ModelState.IsValid)
            {
                if (walletValue > buyQTY*buyPrice)
                {
                    var newWalletValue = walletValue - buyTotal;
                    walletConnection.CashAvailable = newWalletValue;
                    Holding holding = new Holding
                    {
                        Symbol = buySym,
                        AmountHeld = buyQTY,
                        PurchasePrice = buyPrice,
                        Username = user
                    };
                    _context.Add(holding);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Buy));
                }
            }
            return Content("<html><body>Insufficient Credit.</body></html>", "text/html");
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("UserId,Username,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                HttpContext.Session.SetString(loggedIn1, user.Username);
                return RedirectToAction(nameof(Welcome));
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string username, string password)
        {
            if (ModelState.IsValid)
            {
                var userExists = await _context.Users.SingleOrDefaultAsync(m => m.Username == username && m.Password == password);
                if (userExists != null)
                {
                    HttpContext.Session.SetString(loggedIn1, username);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    Debug.WriteLine("Username Not Found");
                }
            }
            return RedirectToAction(nameof(Login));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Begin()
        {
            Wallet newUser = new Wallet();
            newUser.Username = HttpContext.Session.GetString(loggedIn1);
            newUser.CashAvailable = 10000.00;
            _context.Add(newUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        [HttpPost, ActionName("SellCrypto")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SellCrypto(int id)
        {
            var sale = await _context.Holdings.SingleOrDefaultAsync(m => m.PurchaseId == id);
            var walletConnection = await _context.Wallets.SingleOrDefaultAsync(m => m.Username == HttpContext.Session.GetString(loggedIn1));
            var newWalletTotal = sale.PurchasePrice + walletConnection.CashAvailable;
            walletConnection.CashAvailable = newWalletTotal;
            _context.Holdings.Remove(sale);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Sell));
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }
    }
}
